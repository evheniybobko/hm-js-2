let userName;
let userAge;

userName = prompt("What's your name?");
userAge = Number(prompt("How old are you?"));

while(userName === "" ){
    userName = prompt("Try writing your name again");
}

while(userAge === 0 || userAge === "" || isNaN(userAge)){
    userAge = Number(prompt("Try writing your age again"));
}

if(userAge < 18){
    alert("You are not allowed to visit this website");
}
else if(userAge >= 18 && userAge <= 22){
    let confirmAge = confirm("Are you sure you want to continue?");
    if(confirmAge === true){
        alert("Welcome, " + userName + "!");
    }
    else if(confirmAge === false){
        alert("You are not allowed to visit this website");
    }
}
else{
    alert("Welcome, " + userName + "!");
}




/* Які існують типи даних у Javascript?
JavaScript має 8 основних типів:

1. number для будь-яких чисел: цілих чисел з плаваючою точкою, цілі численні значення обмежені діапазоном ±2^53;
2. bigint для цілих чисел довільної довжини;
3. string для рядків. Рядок може містити один або більше символів, немає окремого символьного типу;
4. Boolean для true/false;
5. null для невідомих значень – окремий тип, що має одне значення null;
6. undefined для неприсвоєних значень – окремий тип, що має одне значення undefined;
7. object для складніших структур даних;
8. symbol для унікальних ідентифікаторів. */


/* У чому різниця між == і ===?

1. == це звичайне порівняння
2. === це строге порівняння */


/* Що таке оператор?
Оператор це просто внутрішня функція JavaScript.
При використанні того чи іншого оператора ми просто запускаємо ту чи іншу вбудовану функцію, яка виконує якісь певні дії і повертає результат. */